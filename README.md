# Boilerplate - NestJS, Prisma, Auth JWT e Refresh Token\*

<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

## Description

Este Boilerplate, implementa, a autenticação JTW com Refresh Token no NestJS e Prisma.

`*` Em breve

## Instalação

```bash
$ yarn
```

## Executando

```bash
# development
$ yarn start:dev
```

## Requisitos para uso deste boilerplate

```bash
Ter uma base populada com os dados necessários para autenticação.
```

## Documentação da API

[http://localhost:3333/api](http://localhost:3333/api)
