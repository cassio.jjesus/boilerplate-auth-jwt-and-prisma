export class User {
  id: number;
  name: string;
  email: string;
  phone: string;
  password: string;
  avatar?: string;

  created_at?: Date;
  updated_at?: Date;
}
