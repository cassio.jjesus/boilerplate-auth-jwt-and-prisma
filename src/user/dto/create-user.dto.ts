import { IsEmail, IsString } from 'class-validator';

export class CreateUserDto {
  /**
   * O e-mail será utilizado para fazer login na plataforma
   * @example email@email.com
   */
  @IsEmail()
  email: string;

  @IsString()
  password: string;

  @IsString()
  name: string;

  /**
   * Insira o telefone com o DDD.
   * @example 11999999999
   */
  @IsString()
  phone: string;
}
