export interface UserToken {
  access_token: string;
  user_id: number;
}
