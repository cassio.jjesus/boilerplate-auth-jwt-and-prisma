import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.enableCors();
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  const config = new DocumentBuilder()
    .setTitle('Boilerplate - NestJS, Prisma, Auth JWT e Refresh Token*')
    .setDescription(
      'Este Boilerplate, implementa, a autenticação JTW com Refresh Token no NestJS e Prisma. \n \n `*` Em breve',
    )
    .setVersion('1.0')
    // .addTag('users')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document, {
    customfavIcon: '/favicon.ico',
    customSiteTitle: 'Boilerplate',
    customCss: `
      .topbar-wrapper img {
        content:url(\'../assets/documentation_icon.png\');
        width:auto;
        height:56px;
      }
      .swagger-ui .topbar {
        background-color: #5b90bfff;
      }
    `,
  });

  await app.listen(3333);
}
bootstrap();
